## [0.2.0] - 2020-07-03
* Better detect if you are running in a single tab

## [0.1.0] - 2020-06-25

* First release
